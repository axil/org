List of things to pack when traveling

## Papers and documents

- [ ] Passport
- [ ] ESTA
- [ ] Driver's license
- [ ] Addresses of where to stay

## Tech stuff

- [ ] Laptop (+charger)
- [ ] Mobile (+charger)
- [ ] Socket (US/EU)
- [ ] Camera (+charger)
- [ ] Baterry pack
- [ ] Watch (+usb)
- [ ] Go pro / Yicam