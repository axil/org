#!/bin/bash

VALE_RULE=$1
STAGENAME=$2
VALE_FILE="doc/.vale/gitlab/$VALE_RULE.yml"

if [ -z "$VALE_RULE" ]; then
  echo "No Vale rule provided. Exiting."
  echo "Usage: $0 <vale_rule> <stage>"
  echo "Example: $0 Enablement FutureTense"
  exit 1
fi

if [ -z "$STAGENAME" ]; then
  echo "No stage provided. Exiting."
  echo "Usage: $0 <vale_rule> <stage>"
  echo "Example: $0 Enablement FutureTense"
  exit 1
fi

if ! [ -f "$VALE_FILE" ]; then
  echo -e "'$VALE_FILE' doesn't exist.\nMake sure you're in the GitLab root directory and that you provided the correct rule name.\nExiting."
  exit 1
fi

sed -i 's/level: warning/level: error/' doc/.vale/gitlab/$VALE_RULE.yml
grep -riI --files-with-match "stage: $STAGENAME" . | xargs vale --no-wrap --minAlertLevel error
git checkout doc/.vale/gitlab/$VALE_RULE.yml
